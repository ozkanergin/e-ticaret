﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UrunKatalog.Web.Entity;

namespace UrunKatalog.Web.Identity
{
    public class IdentityInitializer : CreateDatabaseIfNotExists<IdentityDataContext> /*DropCreateDatabaseAlways<DataContext>*/
    {
        protected override void Seed(IdentityDataContext context)
        {

            if(!context.Roles.Any(i => i.Name == "admin"))
            {
                var store   = new RoleStore<ApplicationRole>(context);
                var manager = new RoleManager<ApplicationRole>(store);

                var role = new ApplicationRole("admin", "yönetici rolü")
                { Name="admin"
                ,Description="admin rolu"};
                manager.Create(role);
            }

            if(!context.Roles.Any(i => i.Name == "user"))
            {
                var store   = new RoleStore<ApplicationRole>(context);
                var manager = new RoleManager<ApplicationRole>(store);

                var role = new ApplicationRole()
                {
                    Name = "user",
                    Description = "user rolu"
                }; 
                manager.Create(role);
            }


            if(!context.Users.Any(i => i.Name == "ozkanergin"))
            {
                var store   = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);

                var user = new ApplicationUser() { Name="Özkan" , Surname="Ergin", UserName="ozkanergin",
                Email="ozkanergin10@hotmail.com"};
                manager.Create(user,"1234567");
                manager.AddToRole(user.Id, "admin");
                manager.AddToRole(user.Id, "user");
            }



            if (!context.Users.Any(i => i.Name == "caglaergin"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);

                var user = new ApplicationUser()
                {
                    Name = "Cagla",
                    Surname = "Ergin",
                    UserName = "caglaergin",
                    Email = "caglaerginergin@hotmail.com"
                };
                manager.Create(user, "1234567");
                manager.AddToRole(user.Id, "user");
            }
            base.Seed(context);

        }

    }
}