﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UrunKatalog.Web.Entity;
using UrunKatalog.Web.Models;

namespace UrunKatalog.Web.Controllers
{
    public class HomeController : Controller
    {
        DataContext context = new DataContext();

        // GET: Home
        public ActionResult Index()
        {
            var urunler = context.Products.Where(i => i.IsHome == true && i.IsApproved == true)
                .Select(i => new ProductModel()
                {
                    Id = i.Id,
                    Name = i.Name,
                    Description = i.Description.Length>50 ? i.Description.Substring(0,47) + "..." : i.Description,
                    Price = i.Price,
                    Stock = i.Stock,
                    Image = i.Image ?? "soru.jpg",
                    CategoryId = i.CategoryId
                }).ToList();
            return View(urunler);
        }
        
        // GET: Home
        public ActionResult Details(int id)
        {
            return View(context.Products.ToList().Where(i => i.Id == id).FirstOrDefault());
        }


        // GET: Home
        public ActionResult List(int? id)
        {
            var urunler = context.Products.Where(i => i.IsApproved == true)
                .Select(i => new ProductModel()
                {
                    Id = i.Id,
                    Name = i.Name,
                    Description = i.Description.Length > 50 ? i.Description.Substring(0, 47) + "..." : i.Description,
                    Price = i.Price,
                    Stock = i.Stock,
                    Image = i.Image ?? "soru.jpg",
                    CategoryId = i.CategoryId
                }).AsQueryable();

            if(id != null)
            {
                urunler = urunler.Where(i => i.CategoryId == id);
            }

            return View(urunler.ToList());
        }

        public PartialViewResult GetCategories()
        {
            return PartialView();
        }
    }
}